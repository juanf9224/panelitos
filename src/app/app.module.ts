import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { ReactiveFormsModule } from '@angular/forms';
import { Storage } from '@ionic/storage';
//import { AmChartsDirective } from "amcharts3-angular2/amcharts.directive";

//providers
import { LoginService } from '../providers/login-service/login-service';
import { SocialProvider } from '../providers/social-provider/social-provider';
import { DashboardProvider } from '../providers/dashboard-provider/dashboard-provider';
import { CreateAccountService } from '../providers/create-account-service/create-account-service';
import { MyErrorHandler } from '../providers/error-handler-service/my-error-handler';
import { CustomStylesProvider } from '../providers/custom-style-provider/custom-style-provider';
import { IonicStorageProvider } from '../providers/storage-provider/ionic-storage-provider';
import { ConnectivityService } from '../providers/connectivity-service/connectivity-service';
import { SaveUserProvider } from '../providers/save-user-provider/save-user-provider';
import { MenuDataProvider } from '../providers/menu-data/menu-data';
import { TemplateService } from '../providers/template-service/template-service';
import { FacebookMockProvider } from '../providers/facebook-mock/facebook-mock';
import { TableService } from '../providers/table-service/table-service';
import { TableFormService } from '../providers/table-form-service/table-form-service';
import { GraphService } from '../providers/graph-service/graph-service'; 

//Pipes...
import { TrackingPipe } from '../pipes/tracking-pipe';
import { KeysPipe } from '../pipes/keys-pipe';
import { SafeHtmlPipe } from '../pipes/dom-sanitize';

//CIAUSE
import { CiaUseConfig } from '../providers/veo-config-provider/ciause';

//Component
import { App } from './app.component';

//Pages
import { LoginPage } from '../pages/login-page/login-page';
import { AddUserPage } from '../pages/add-user-page/add-user-page';
import { TablePage } from '../pages/table-page/table-page';
import { TableFormPage } from '../pages/table-form-page/table-form-page';
import { GraphicPage } from '../pages/graphic-page/graphic-page';
import { DashboardPage } from '../pages/dashboard-page/dashboard-page';
import { CreateAccountPage } from '../pages/create-account/create-account';

@NgModule({
  declarations: [
    App,
    LoginPage,
    AddUserPage,
    TrackingPipe, 
    KeysPipe,
    SafeHtmlPipe,
    DashboardPage,
    TablePage,
    TableFormPage,
    GraphicPage,
    CreateAccountPage
  ],
  imports: [
    IonicModule.forRoot(App),
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    App,
    LoginPage,
    AddUserPage,
    DashboardPage,
    TablePage,
    TableFormPage,
    GraphicPage,
    CreateAccountPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: MyErrorHandler}, 
    CiaUseConfig, 
    CustomStylesProvider, 
    Storage,
    LoginService, 
    SaveUserProvider, 
    SocialProvider, 
    CreateAccountService,
    IonicStorageProvider, 
    ConnectivityService, 
    DashboardProvider, 
    TemplateService,
    MenuDataProvider,
    FacebookMockProvider,
    TableService,
    TableFormService,
    GraphService
    ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ]
})
export class AppModule {}
