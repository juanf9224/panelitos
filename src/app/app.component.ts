import { Component, ViewChild, enableProdMode } from '@angular/core';
import { Platform, Nav, MenuController, Events, ToastController, LoadingController, AlertController, Alert } from 'ionic-angular';
import { StatusBar } from '@ionic-native/StatusBar';
import { AppVersion } from '@ionic-native/AppVersion';
import { Splashscreen } from '@ionic-native/SplashScreen';
import { InAppBrowser } from '@ionic-native/InAppBrowser';
import 'rxjs/Rx';

//providers...
import { IonicStorageProvider } from '../providers/storage-provider/ionic-storage-provider'; 
import { ConnectivityService} from '../providers/connectivity-service/connectivity-service';
import { CustomStylesProvider} from '../providers/custom-style-provider/custom-style-provider';
import { SocialProvider } from '../providers/social-provider/social-provider';
import { FacebookMockProvider } from '../providers/facebook-mock/facebook-mock';

// Pages...
import { LoginPage } from '../pages/login-page/login-page';
import { AddUserPage } from '../pages/add-user-page/add-user-page';
import { DashboardPage } from '../pages/dashboard-page/dashboard-page';

//Enable Production mode...
enableProdMode();

export interface PageObj {
  title: string;
  component: any;
  icon: string;
  action: string;
  firstTime: boolean
}

@Component({
  templateUrl: 'app.html'
})
export class App {
  
  @ViewChild(Nav) nav: Nav;
  customStyles: any;
  loggedIn: boolean;

  appPages: PageObj[] = [
      //{ title: 'Dashboard', component: DashboardPage, icon: 'menu', action: 'nav'},
      { title: 'Iniciar sesión', component: LoginPage, icon: 'log-in', action: 'login', firstTime: false},
      //{ title: 'Crear cuenta', component: CreateAccountPage, icon: 'paper', action: 'nav', firstTime: false}
      //{ title: 'Servicios', component: ServiceListPage, icon: 'clipboard', action: 'nav'},
      //{ title: 'Oficinas', component: OfficePage, icon: 'map', action: 'nav'}
    ]; 

  loggedInPages: PageObj[] = [
      { title: 'Dashboard', component: DashboardPage, icon: 'menu', action: 'nav', firstTime: false},
      { title: 'Agregar Referidos', component: AddUserPage, icon: 'person-add', action: 'nav', firstTime: false},           
      //{ title: 'Servicios', component: ServiceListPage, icon: 'clipboard', action: 'nav'},      
      //{ title: 'Oficinas', component: OfficePage, icon: 'map', action: 'nav'},
      { title: 'Cerrar sesión', component: LoginPage, icon: 'log-out', action: 'logout', firstTime: false}
    ];

  rootPage: any;
  client: any = {
      name: 'Panelitos',
      username: ''
    };
  appVersion: any = '0.0.0';  
  loggedInState: boolean;
  public static NEW_LOGIN; boolean;
  loader: any;

  constructor(
    public platform: Platform, 
    public menu: MenuController, 
    public events: Events, 
    public storageProvider: IonicStorageProvider, 
    public toastCtrl: ToastController, 
    public loadingCtrl: LoadingController, 
    public alertCtrl: AlertController,
    public connectivityService: ConnectivityService,
    public customStylesProvider: CustomStylesProvider,
    public socialProvider: SocialProvider,
    public fbMock: FacebookMockProvider) {

    this.platform.ready().then(() =>{
             
            this.initializeApp();
            this.loadCustomStyles();
            this.checkLoginStatus();
            this.listenToEvents(); 

          })
          .catch(
            error =>{
              console.error('Error initialising app', error);
              this.showAlert('Error initialising app');
          });  
  }

  //Inicializar aplicacion...
  initializeApp() {
    if(this.connectivityService.isOnline()){
          console.log('isOnline');
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      console.log('platform ready');   
         
      if(this.platform.is('cordova') || this.platform.is('ios') || this.platform.is('android')){
            console.log('cordova or iOS or Android', this.platform.is('cordova'), this.platform.is('ios'), this.platform.is('android'));        
               
              setTimeout(() =>{
                try{
                    StatusBar.styleDefault();
                    StatusBar.backgroundColorByHexString('#222');                     
                    Splashscreen.hide();       
                }catch(error){
                  console.error('Error: ', error);
                  this.showAlert('Error loading app');
                }
               }, 300); 

              AppVersion.getVersionNumber().then(version => {
              this.appVersion = version;
              }).catch(error => {
                console.error('Error: ', error);
              }); 
        }      

      this.loader = this.loadingCtrl.create({
        content: 'Iniciando'
      });  

      this.loader.present();
    });
        }else{
            //console.log('isOffline ');
            Splashscreen.hide();    
            this.showAlert('Sin conexión').present().then(() => {

                this.platform.exitApp();

            });
        }
  }

  checkLoginStatus(){

   //if(this.platform.is('cordova')){

        //this.socialProvider.fbLoginStatus()
        this.fbMock.apiCall('fb-login-status.json')
        .subscribe(
          data =>{
                  console.log('Facebook Login Status response: '+ JSON.stringify(data));

                  if(data.status === 'connected'){
                    console.log('validate facebook token');
                    this.storageProvider.loadData('fbLoginState').then(
                      savedD =>{
                        let savedData:any = JSON.parse(savedD);
                        console.log('savedData: '+ savedD);   
                                        
                        if(savedData === undefined || savedData === "null" || savedData === null || savedData.accessToken == undefined || savedData.accessToken !== data.authResponse.accessToken){  

                          console.log('diferent or undefined access token...');                      
                          this.storageProvider.logout();
                          //this.socialProvider.fbLogout();
                          this.enableMenu(false);
                          this.decideFirstPage(false);

                        }else{
                          this.loggedIn = savedData.loginStatus;
                          console.log('valid access token', savedData, this.loggedIn === true);
                          setTimeout(() => {
                                  this.enableMenu(this.loggedIn === true);
                                  this.decideFirstPage(this.loggedIn === true);          
                                }, 500);    

                          this.getClientInfo(this.loggedIn === true);
                        }
                    });
                  }else if(data.status === 'unknown'){
                        console.log('status unknown');
                        this.storageProvider.logout();
                        this.enableMenu(false);
                        this.decideFirstPage(false);
                  }else{
                        console.log('Not loggedIn');

                        setTimeout(() => {
                                this.enableMenu(false);
                                this.decideFirstPage(false);          
                              }, 500);    

                        this.getClientInfo(false);             
                  }
          },
          error => {
                console.error('Facebook Login Status error: '+ JSON.stringify(error));
                this.events.publish('dismissLoader', 'error');
          }
        );
   /*}else{
     setTimeout(() => {
          this.enableMenu(true);
          this.decideFirstPage(this.loggedIn === true);          
        }, 500); 
   }*/
  }

  loadCustomStyles(){
    this.customStylesProvider.load().subscribe(
      data => { 
                //console.log('success in getting data');
                this.customStyles = data._body;
                let head = document.head || document.getElementsByTagName('head')[0];
                let styleTag = document.createElement('style');
                styleTag.type = 'text/css';
                styleTag.appendChild(document.createTextNode(this.customStyles));
                //console.log('styles ', this.customStyles);
                head.appendChild(styleTag);                
              },
      error =>{
                console.error('Error in getting custom styles ', error);
              }
    );
  }

  decideFirstPage(isLogged: boolean){
      console.log('Deciding first page');
      if(isLogged){
          //console.log('Mis Paquetes');
          this.loggedInPages[2].firstTime = true;
          this.nav.setRoot(DashboardPage);
      }else{      
          this.loggedInPages[0].firstTime = true;
          this.nav.setRoot(LoginPage);
      }
    
  }

// Obtener datos del cliente...
  getClientInfo(loggedIn: boolean){

    if(loggedIn){
        
      this.storageProvider.loadData('UserData').then(client => {
        
            let data = JSON.parse(client);            
            console.log('user data: ', JSON.stringify(data));
            if(data !== null || data !== undefined && data.provider === 'facebook'){
              this.client = {
                name: data.nombre,
                username: ''
              }
            }else{            
              let nameArr = data.Nombre.split(' ');
              let lastName = nameArr[2] || nameArr[1] || '';
              this.client = {
                name: nameArr[0] + ' ' + lastName,
                username: data.username
              }
            }
          //console.log('Client:',this.client.name, this.client.username);
      }).catch(error =>{
          console.error('Error loading user dara: ', error);
      })
    }
  }

  openPage(page: PageObj) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.action === 'newsBrowser'){
      
      //Open news page from djpack.com      
      let browser = new InAppBrowser('http://url');
      browser.show();

      browser.on('exit').subscribe(() =>{
        //console.log('exit event...');
        browser.close();
      });

    }else if(page.action === 'logout'){       
      //console.log('logout');
      this.events.publish('presentLoader', 'logout');
      setTimeout(() => {
        this.storageProvider.logout();
        this.socialProvider.fbLogout();
        this.nav.setRoot(page.component);
      }, 500);
    }else{      
      if(!page.firstTime){
        this.events.publish('presentLoader', 'loading');
        }
        setTimeout(() =>{
          this.nav.setRoot(page.component);
        }, 300);           
    }
  }


  //App global events...
  listenToEvents(){
    // Login event listener...
    this.events.subscribe('user:loginFB', (username) => {
      //console.log('Login event...');

      App.NEW_LOGIN = true;
      
      this.events.publish('dismissLoader');

      let toast = this.toastCtrl.create({
          message: 'Iniciaste sesión con Fb: '+ username,
          duration: 3000,
          position: 'bottom'
            });
      toast.present();

      this.menu.enable(true, 'logged-in-menu');
    });

    // Logout event listener...
    this.events.subscribe('user:logout', () =>{
      //console.log('Logout event...');
      //this.enableMenu(false);
      this.menu.enable(true, 'logged-out-menu');
    });

    // presentLoader event listener
    this.events.subscribe('presentLoader', (content: string) => {
        //console.log('Present Loader: '+ param);
          //console.log('param: '+ param);
          this.loader = null;
          this.loader = this.loadingCtrl.create({
            content: content || 'Cargando'
          });
          this.loader.present();
    });

    this.events.subscribe('dismissLoader', (title?:string, msg?: string, showAlert?: boolean) => {
        console.log('Dismiss Loader ', msg);
        this.loader.dismiss();
        if(showAlert){
          this.showAlert('',msg).present();
        }
    });

    this.events.subscribe('loginState', (param) => {
      console.log('loginState event');
      this.loggedIn = param;
      console.log('loginState: ', this.loggedIn);
    });
  }

  showAlert(title?: string, msg?: string): Alert{
    let alert = this.alertCtrl.create({
      title: title || 'Lo sentimos',
      subTitle: msg || 'Hubo un error en la solicitud, verifica tu conexión e intentalo de nuevo.',
      cssClass: 'alert-style',
      buttons: ['OK']
    });

    return alert;
  }

  //Enable logged in menu or logged out menu
  enableMenu(loggedIn){
    //console.log('Current loggedIn: '+ loggedIn);
    this.menu.enable(loggedIn, 'logged-in-menu');
    this.menu.enable(!loggedIn, 'logged-out-menu');
  }

  //check if menu is opened
  menuOpened(){
    this.events.publish('menu:opened');    
  }

  //check if menu is closed
  menuClosed(){
    this.events.publish('menu:closed');
  }
}
