import { Injectable, Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'keys'
})
@Injectable()
export class KeysPipe implements PipeTransform{
  keys: any;
    transform(value, args?:string[]) : any {
    try{
    //console.log('data', JSON.stringify(value));
    this.keys = [];
    //let recepcion = [];
    for (let val in value) {           
        this.keys.push({key: val, value: value[val]});  
    }
     //console.log('keys', JSON.stringify(this.keys));
    return this.keys;
    }catch(error){
      console.log(error);
      return null;
    }
  }
}

