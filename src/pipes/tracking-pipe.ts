import { Injectable, Pipe, PipeTransform } from '@angular/core';

/*
  Generated class for the TrackingPipe pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'trackingPipe'
})
@Injectable()
export class TrackingPipe implements PipeTransform{
  /*
    Takes a value and makes it lowercase.
   */
  trackingPipe: any;
  transform(value, args?:string[]) : any {
     try{
      this.trackingPipe = [];
      let recepcion = {};
      let entrega = {};
        for (let key in value) {
          if(key === 'Recepción'){
            recepcion = {key: key, value: value[key]};
          }
          if(key === 'Entrega'){
            entrega = {key: key, value: value[key]};
          }
          if(key.indexOf('Recepción ') > -1){
            this.trackingPipe.push({key: key, value: value[key]});         
            }else{
            }
          }
        this.trackingPipe.push(recepcion, entrega);
        return this.trackingPipe;
        }catch(error){
          console.log(error);
        }
  }
}
