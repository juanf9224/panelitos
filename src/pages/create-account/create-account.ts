import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, Events, AlertController, Slides, NavParams, Nav } from 'ionic-angular';
import { Facebook } from '@ionic-native/Facebook';
import { IonicStorageProvider } from '../../providers/storage-provider/ionic-storage-provider';
import { getBaseUrl, getUrlCrc } from '../../providers/veo-config-provider/ciause';
import { FacebookMockProvider } from '../../providers/facebook-mock/facebook-mock';
import { CreateAccountService } from '../../providers/create-account-service/create-account-service';

import { DashboardPage } from '../dashboard-page/dashboard-page';
 
@Component({
  selector: 'page-create-account',
  //templateUrl: 'create-account.html',
  templateUrl: getBaseUrl('ADDUSER') + '&UCRC=' + getUrlCrc('ADDUSER'),
})

export class CreateAccountPage implements OnInit{

    @ViewChild('signupSlider') signupSlider: Slides;
    
    templateTitle: string = 'Crear Cuenta';
    fbUID: any;
    fbUserDetail: any;
    slideForms: any;
    slideOneForm: FormGroup;
    slideTwoForm: FormGroup;
    slideThreeForm: FormGroup;
    slideFourForm: FormGroup;
    callDays: any = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
    selectOptions: {};
    pickerOpts: {};
    ultData: any;
    globalErrorMsg: string = '';
    submitAttempt: boolean = false;
    slideOneNext: boolean = false;
    slideThreeNext: boolean = false;
    slidesInstance: any;
 
    constructor(
        public navCtrl: NavController, 
        public formBuilder: FormBuilder, 
        public event: Events, 
        public alertCtrl: AlertController, 
        public navParams: NavParams,
        public nav: Nav,
        public storageProvider: IonicStorageProvider,
        public fbMock: FacebookMockProvider,
        public createAccService: CreateAccountService) {       
            this.fbUID = navParams.get('fbUID');
    }

    ngOnInit(){    
        this.signupSlider.paginationType = 'progress'; 
        this.signupSlider._allowSwipeToNext = true;
        this.signupSlider._allowSwipeToPrev = true;
        this.selectOptions = {
            title: 'Día(s) preferible(s) de llamada (Opcional)1',
            cssClass: 'select-box-style'
        };

        this.pickerOpts = {
            title: 'Escoja las horas disponibles',
            cssClass:'picker-opt-style'
        }        

        this.event.publish('dismissLoader');

        this.slideOneForm = this.formBuilder.group({
            firstName: ['', Validators.compose([Validators.minLength(4), Validators.pattern('^[A-Za-z -]*[A-Za-z][A-Za-z -]*$')])],
            lastName: ['', Validators.compose([Validators.minLength(4), Validators.pattern('^[A-Za-z -]*[A-Za-z][A-Za-z -]*$')])],
            pueblo: ['', Validators.compose([Validators.minLength(4), Validators.pattern('^[A-Za-z -]*[A-Za-z][A-Za-z -]*$')])],
        });

        this.slideTwoForm = this.formBuilder.group({
            callDay: ['',Validators.compose([Validators.pattern('^[A-Za-z -\,]*[A-Za-z][A-Za-z -\,]*$')])],
            deHora: ['',Validators.compose([Validators.pattern(/^((\d{1,2}?)|(\d{1})):\d{1,2}$/)])],
            aHora: ['',Validators.compose([Validators.pattern(/^((\d{1,2}?)|(\d{1})):\d{1,2}$/)])]
        });

        this.slideThreeForm = this.formBuilder.group({
            telPrincipal: ['',Validators.compose([ Validators.minLength(7), Validators.maxLength(10), Validators.pattern('^[0-9 -]*[0-9][0-9 -]*$')])],
            telSecundario: ['',Validators.compose([Validators.minLength(7), Validators.maxLength(10), Validators.pattern('^[0-9 -]*[0-9][0-9 -]*$')])],
            telCel: ['',Validators.compose([Validators.minLength(7), Validators.maxLength(10), Validators.pattern('^[0-9 -]*[0-9][0-9 -]*$')])],
            telCasa: ['',Validators.compose([Validators.minLength(7), Validators.maxLength(10), Validators.pattern('^[0-9 -]*[0-9][0-9 -]*$')])]
        });

        this.slideFourForm = this.formBuilder.group({
            emailPrincipal: ['', Validators.compose([Validators.pattern(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)])],
            emailSecundario: ['', Validators.compose([Validators.pattern(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)])],
            emailRepresentante: ['', Validators.compose([Validators.pattern(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)])]
        });

        this.slideForms = [this.slideOneForm, this.slideTwoForm, this.slideThreeForm, this.slideFourForm];

    }

    isSlideNext(activeIndex: number, prevIndex: number): boolean{
        if(activeIndex > prevIndex){
            return true;
        }else{
            return false;
        }
    }
 
    next(slide){
        if(slide === 1){
            this.slideOneNext = true;
        }else{
            this.slideThreeNext = true;
        }
        
        this.signupSlider.slideNext();        
    }
 
    prev(){
        this.signupSlider.slidePrev();
    }

    slideWillChange(event, swiper){
        let actIndex = this.signupSlider.getActiveIndex();
        let prevIndex= this.signupSlider.getPreviousIndex();
        console.log('transition on slide: ', actIndex, prevIndex);
        if(this.isSlideNext(actIndex, prevIndex)){
            console.log('is next slide');
            this.validateSlide(prevIndex);
        }else{
            console.log('is prev or same slide');
        }      
    }

    validateSlide(slideIndex: number){
        try{

        if(slideIndex === 0){
        this.slideOneNext = true;
        }else if(slideIndex === 2){
            this.slideThreeNext = true;
        }

        if(!this.slideForms[slideIndex].valid){
            console.log('invalid');
            this.alertCtrl.create({
                title: 'Aviso',
                message: 'Quedaron campos vacios o invalidos.',
                buttons: [
                    {
                        text: 'Solucionar',
                        handler: () =>{
                            console.log('solucionar');
                            this.signupSlider.slidePrev();
                        }
                    },
                    {
                        text: 'Continuar',
                        handler: () =>{
                            console.log('Continuar');
                        }
                    }
                    ]
            }).present()
        }else{
            console.log('valid');
        }
        }catch(error){
            console.error('Error: ', error);
        }
    }

    checkValidation(){
        this.submitAttempt = true;
        let validCount = 0;
        for(let i = 0; i < this.slideForms.length; i++){
            let slideForm = this.slideForms[i];
            if(!slideForm.valid){
                console.log('invalid: ', i);
                this.signupSlider.slideTo(i);
                break;
            }else{
                validCount++;
                console.log('valid ', i);
                if(validCount >= this.slideForms.length){
                    this.save();
                } 
            }
        }  
    }

    private clearForm(){
        
            this.slideOneForm.reset();
            this.slideTwoForm.reset();
            this.slideThreeForm.reset();
            this.slideFourForm.reset();
            this.signupSlider.slideTo(0);
    }
 
    save(){
        this.event.publish('presentLoader', 'Guardando');
        this.submitAttempt = true;
        this.ultData = {
            FBUID: {FBUID: this.fbUID.userID},
            datosGenerales: this.slideOneForm.value,
            prefLlamadas: this.slideTwoForm.value,
            telefonos: this.slideThreeForm.value,
            emails: this.slideFourForm.value
        }
        let calls = this.ultData.prefLlamadas.callDay == undefined || this.ultData.prefLlamadas.callDay === '' ? '' : this.ultData.prefLlamadas.callDay.join(',');
        //let days = '';
        console.log(calls)

        let prettyPrint = 
        '<b>Nombre:</b> ' + this.ultData.datosGenerales.firstName +
        '<br> <b>Apellido:</b> '+ this.ultData.datosGenerales.lastName + 
        '<br> <b>Pueblo:</b> '+ this.ultData.datosGenerales.pueblo +
        '<br> <b>Dias preferibles:</b> '+ calls +
        '<br> <b>De:</b> '+ this.ultData.prefLlamadas.deHora + 
        '<br> <b>A:</b> ' + this.ultData.prefLlamadas.aHora + 
        '<br> <b>Telefono Principal:</b> '+ this.ultData.telefonos.telPrincipal +
        '<br> <b>Telefono Secundario:</b> ' + this.ultData.telefonos.telSecundario +
        '<br> <b>Telefono Celular:</b> ' + this.ultData.telefonos.telCel +
        '<br> <b>Telefono Casa:</b> ' + this.ultData.telefonos.telCasa +
        '<br> <b>Email Principal:</b> ' + this.ultData.emails.emailPrincipal +
        '<br> <b>Email Secundario:</b> ' + this.ultData.emails.emailSecundario +
        '<br> <b>Email Representante:</b> ' + this.ultData.emails.emailRepresentante;

            let alertResult = this.alertCtrl.create({
                title: 'Tu información',
                subTitle: prettyPrint,
                cssClass: 'alert-box',
                buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () =>{
                        console.log('Operation canceled...');
                    }
                },
                {
                    text: 'Confirmar',
                    handler: () =>{
                        
                        this.createAccService.saveUser(this.ultData).subscribe(
                            success => {
                                let userData = success.responseObject[0];
                                console.log('sucess saving user');                        
                                console.log('FB user linked... proceed to login');   
                                this.loginFb(userData);
                            },
                            error =>{
                                this.event.publish('dismissLoader', 'Algo salío mal!', 'Error: '+JSON.stringify(error), true);
                                console.error('error saving user');
                            }
                        );
                    }
                }
                ]
            });

            alertResult.present();     

            console.log('ultData: ', JSON.stringify(this.ultData));
    }

  loginFb(userData: any){      
        //Facebook.login(['email']).then(
            this.fbMock.apiCall('fb-login-data.json').subscribe(
            data => {
                console.log('FB login data: '+ JSON.stringify(data));

                let fbLogin = {
                  accessToken: data.authResponse.accessToken,
                  sessionKey: data.authResponse.session_key,
                  userID: data.authResponse.userID,
                  loginStatus: true 
                }; 

                return this.createAccService.checkIfUserExist(fbLogin).subscribe(
                        data =>{
                            if(data.totalrecord < 1){
                                console.log('FB user not linked to any veoCRM user...');
                                let alert = this.alertCtrl.create({
                                            title: 'Cuenta No Asociada',
                                            subTitle: 'Esta cuenta de Facebook es distinta de la asociada al usuario creado, inicia sesión en Facebook con la cuenta asociada o crea otro usuario para esta cuenta de Facebook.',
                                            cssClass: 'alert-style',
                                            buttons: [
                                                        {
                                                            text: 'Cancelar',
                                                            role: 'cancel',
                                                            handler: () =>{
                                                                console.log('Operation canceled...');
                                                            }
                                                        },
                                                        {
                                                            text: 'Crear cuenta',
                                                            handler: () =>{
                                                                this.clearForm();
                                                            }
                                                        }
                                                        ]
                                            });
                                alert.present();
                                
                            }else{                              
                                console.log('FB user linked... proceed to login');                              
                                this.storageProvider.loginFb(fbLogin);
                                this.saveFbUserData(userData);
                            }
                        });     
            },
            error => {
                console.log('FB login error: '+ JSON.stringify(error));
                this.event.publish('dismissLoader', 'Algo salío mal!', JSON.stringify(error), true);
            }
        );
  }

  saveFbUserData(userData: any){
    
        //Facebook.api('me?fields=email,name', ['public_profile']).then(
            this.fbMock.apiCall('fb-user-data').subscribe(
            data => {              
                let fbUserData = {
                  username: data.email, 
                  nombre: data.name,
                  id: userData.ID, 
                  fbId: data.id,
                  provider: 'facebook',
                  password: ''
                };
                console.log('FB profile data with casoid: '+ JSON.stringify(data));
                    this.fbUserDetail = fbUserData;
                    this.storageProvider.saveData(this.fbUserDetail);
            
                setTimeout(() => {
                this.navCtrl.setRoot(DashboardPage, {
                    loginRef: true
                });
                }, 400);
            },
            error =>{                
                this.event.publish('dismissLoader', 'Algo salío mal!', JSON.stringify(error), true);
                console.log('FB profile error: '+ JSON.stringify(error));
        });
  } 
 

}
