import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Events, Slides } from 'ionic-angular';
import { TableFormService } from '../../providers/table-form-service/table-form-service';
import { getBaseUrl, getUrlCrc } from '../../providers/veo-config-provider/ciause';

@Component({
  selector: 'table-form-page',
  //templateUrl: 'table-form-page.html'
  templateUrl: getBaseUrl('TABLEFORM') + '&UCRC=' + getUrlCrc('TABLEFORM')
})
export class TableFormPage implements OnInit{
  @ViewChild('infoSlider') infoSlider: Slides;
  estatusItem: string;
  item: any; 
  values: Array<any>;
  valuesConfig: any;
  showCards: boolean = true;
  templateTitle: string;
  paginationType: string = 'bullets';
  colHeaderClass: string;
  showBack: boolean = false;
  userCaso: any;
  constructor(
    public nav: NavController, 
    public navParams: NavParams, 
    public view: ViewController,  
    public events: Events,
    public tableFormService: TableFormService) {  		
  }

  ngOnInit(){
    try{ 
        this.item = this.navParams.get('item');
        this.showBack = this.navParams.get('showBack') || false; 
        this.userCaso = this.navParams.get('referer');
        //this.slideConfig();
        setTimeout(() =>{ 
          this.loadOptions(this.item.CodeURL);
        }, 1000); 
    
  }catch(error){
      console.error(error);
    }    
  }

  loadOptions(param?:string){ 
      this.tableFormService.load(param, this.userCaso).subscribe(
        data => {
          let options = data.responseObject;
          let pfldc = [];
          let pfldj = [];
          let pflda = [];
          pfldc = data.PFLDC.split(',');
          pfldj = data.PFLDJ.split(',');
          pflda = data.PFLDA.split(',');
          this.values = options;

          if(pfldj.length > 2){
            this.colHeaderClass = pfldj.shift();
            console.log('Column header class found... removing... ', pfldj);
          }else{
            this.colHeaderClass = '';
            console.log('no column header class found.', pfldj);
          } 
          //console.log('col headers: ', this.colHeaderClass);
          this.templateTitle = data.PARTIT;
          //this.valuesConfig = this.getValuesConfig(options);
          this.valuesConfig = {classes: pfldc, rowClasses: pfldj};              
          //console.log('config: ', JSON.stringify(this.valuesConfig), JSON.stringify(this.values), this.values.length);         
          let valuesRowClasses = this.valuesConfig.rowClasses;
          //console.log('pager type: ', pflda[0] || '', pflda[1] || ''); 
          //this.slideConfig(pflda[0], pflda[1]);
          this.customTableStyle(valuesRowClasses[0], valuesRowClasses[1], this.valuesConfig.classes);
          
          //console.log('data: ', JSON.stringify(options));
          //this.processItems(options);
        },
        error => {
          console.error('Error laoding options: ', error);
        }
      );    
  }

  slideConfig(pagerType?: string, direction?: string, update?: boolean){
    try{                    
          this.paginationType = pagerType || 'bullets';              
          this.infoSlider.update(); 
          console.log('pager', pagerType, this.infoSlider.paginationType, this.infoSlider.direction);
      
    }catch(error){
      console.error(error);
    } 
  }


  customTableStyle(evenClass?: string, oddClass?: string, colClass?: string){
    setTimeout(() =>{      
    //console.log('colors: ', evenClass, evenClass);
    try{
    let oddElement = document.getElementsByClassName('odd');
    for(let i=0; i <= oddElement.length -1; i++){
        //console.log('odd element', oddElement);
        let ele = <HTMLElement>oddElement.item(i);
        ele.className = 'row-style odd '+ oddClass;
    }

    let evenElement = document.getElementsByClassName('even');
    for(let i=0; i <= evenElement.length -1; i++){
        //console.log('even element', evenElement);
        let ele = <HTMLElement>evenElement.item(i);
        ele.className = 'row-style even '+ evenClass;
    }

    let colStyle = document.getElementsByClassName('col-style');
    let colCount = 0;
    for(let i=0; i <= colStyle.length -1; i++){
      if(colCount > colClass.length -1){
        colCount = 0;
      }
      
      let ele = <HTMLElement> colStyle.item(i);
      ele.className += ' '+ colClass[colCount];
      colCount++
    }
    this.showCards = false;
    }catch(error){
      console.log('Error adding row style: ', error);
    }
    }, 300);
  }

  slideWillChange(event, swiper){
    //let actIndex = this.infoSlider.getActiveIndex();
    //let prevIndex= this.infoSlider.getPreviousIndex();
    //console.log('transition on slide: ', actIndex, prevIndex);     
  }


  dismiss(){
    this.view.dismiss();
  }

}