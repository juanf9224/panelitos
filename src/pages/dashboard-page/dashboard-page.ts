import { Component, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import { NavController, NavParams, LoadingController, Events, Platform } from 'ionic-angular';
import { getBaseUrl, getUrlCrc, CiaUseConfig } from '../../providers/veo-config-provider/ciause';
import { DashboardProvider } from '../../providers/dashboard-provider/dashboard-provider';
import { IonicStorageProvider } from '../../providers/storage-provider/ionic-storage-provider';
import { TemplateService } from '../../providers/template-service/template-service';


@Component({
  selector: 'dashboard-page',
  //templateUrl: 'dashboard-page.html',
  templateUrl: getBaseUrl('DASHBOARD') + '&UCRC=' + getUrlCrc('DASHBOARD'),
  animations: [
  trigger('flyInOut', [
    state('in', style({opacity: 1, transform: 'translate3d(0, 0, 0)'})),
    state('out', style({opacity: 0.5, transform: 'translate3d(-350%, 0, 0)'})),
    transition('in => out', [
      animate('0.5s ease-in')
    ]),
    transition('out => in', [
      animate('0.5s ease-out')
    ])
  ])
  ]
})
export class DashboardPage implements OnInit{
  
  options: any;
  flyInOutState: string = 'out';
  templateTitle: string = 'Dashboard';
  imgSrc: string;
  userCaso: any;

  constructor(
    public nav: NavController, 
    public navParams: NavParams, 
    public loadingCtrl: LoadingController, 
    public events: Events, 
    public dashboardProvider: DashboardProvider, 
    public templateService: TemplateService,
    public platform: Platform,
    public storageProvider: IonicStorageProvider) {

  }

  ngOnInit(){
      this.loadOptions();    
  }

  loadOptions(param?:string){ 
    this.storageProvider.loadData('UserData').then(
      result =>{     
        this.userCaso = JSON.parse(result);   
        this.dashboardProvider.load(param, this.userCaso).subscribe(
          data => {
            let options = data.responseObject;
            //console.log('dashboardData: ', JSON.stringify(options));
            for(let opt in options){
              let imgName = options[opt].ImageURL.substring(options[opt].ImageURL.lastIndexOf('/') , options[opt].ImageURL.length);
              if(options[opt].ImageURL === null || options[opt].ImageURL === undefined || options[opt].ImageURL === '' || imgName === ' '){              
                options[opt].ImageURL = '';                         
                //console.log('imgName blank', imgName, opt, parseInt(opt));
              }else{
                //console.log('imgName not blank', imgName);
                options[opt].ImageURL = CiaUseConfig.V_IMAGE_REPOSITORY + options[opt].ImageURL;
              }
            }
            this.processItems(options);
          }
        );
    },
    error =>{

    } 
    );   
  }

  processItems(options: any){     
      this.options = options;
        setTimeout(() => {
          this.events.publish('dismissLoader');
          //Cambiar estado del efecto a in
          this.flyInOutState = 'in';      
        }, 500);
        
  }

  optionTapped(event, item){
    this.templateService.optionTapped(item).subscribe(
      result =>{
        if(result === false){
          //Cambiar estado del efecto a out
          this.flyInOutState = 'out';   
          setTimeout(() => {
            this.loadOptions(item.CodeURL);     
          }, 300);
        }else{
          //console.log('Result: ', JSON.stringify(result));        
          this.nav.push(result.view, {
                referer: this.userCaso,
                item: result.info
          });
        }          
      },
      error =>{
        console.error('Error: ', error);
      }
    );
}

}