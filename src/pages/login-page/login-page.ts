import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, Platform, LoadingController, NavParams, Events, AlertController} from 'ionic-angular';
import { FormGroup, FormBuilder, Validators /*, AbstractControl*/ } from '@angular/forms';
import { Facebook } from '@ionic-native/Facebook';

//Providers...
import { SocialProvider } from '../../providers/social-provider/social-provider';
import { IonicStorageProvider } from '../../providers/storage-provider/ionic-storage-provider';
import { User } from './user';
import { getBaseUrl, getUrlCrc } from '../../providers/veo-config-provider/ciause';
import { LoginService } from '../../providers/login-service/login-service';
import { FacebookMockProvider } from '../../providers/facebook-mock/facebook-mock';
//Pages...
import { DashboardPage } from '../dashboard-page/dashboard-page';
import { CreateAccountPage } from '../create-account/create-account';


@Component({
  selector: 'login-page',
  //templateUrl: 'login-page.html'
  templateUrl: getBaseUrl('LOGIN') + '&UCRC=' + getUrlCrc('LOGIN')
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;	
  user: User;
  loader: any;
  loginStatus: boolean;
  templateTitle: string = 'Iniciar Sesión';

  constructor(
    public navCtrl: NavController, 
    public events: Events, 
    public storageProvider: IonicStorageProvider, 
    public fb: FormBuilder, 
    public toastCtrl: ToastController, 
    public loadingCtrl: LoadingController, 
    private platform: Platform,
    public loginService: LoginService, 
    public navParams: NavParams,
    public socialProvider: SocialProvider,
    public alertCtrl: AlertController,
    public fbMock: FacebookMockProvider
    ) {}

  ngOnInit(){       
  }

  ionViewDidEnter(){
    setTimeout(() => {
    this.events.publish('dismissLoader');
    }, 300);
  }

   loginWithFb(){
    this.events.publish('presentLoader', null, 'Iniciando Sesión', false);
    console.log('Login with Facebook');
        //Facebook.login(['email']).then(
            this.fbMock.apiCall('fb-login-data.json').subscribe(
            data => {
                console.log('FB login data: '+ JSON.stringify(data));

                let fbLogin = {
                  accessToken: data.authResponse.accessToken,
                  sessionKey: data.authResponse.session_key,
                  userID: data.authResponse.userID,
                  loginStatus: true 
                }; 

                return this.loginService.checkIfUserExist(fbLogin).subscribe(
                    data =>{
                        let userData = data.responseObject[0];
                        console.log('data loginWithFb() => checkIfUserExist(): ', JSON.stringify(userData));
                        if(data.totalrecord < 1){
                            console.log('FB user not linked to any veoCRM user...');
                            let alert = this.alertCtrl.create({
                                        title: 'Crear cuenta',
                                        subTitle: 'Esta cuenta de Facebook no esta asociada a ningun usuario, crea una cuenta para continuar.',
                                        cssClass: 'alert-style',
                                        buttons: ['OK']
                                        });
                            alert.present().then(() =>{
                            this.navCtrl.setRoot(CreateAccountPage, {fbUID: fbLogin});
                            })
                            
                        }else{                              
                            console.log('FB user linked... proceed to login', JSON.stringify(userData));                              
                            this.storageProvider.loginFb(fbLogin);
                            this.saveFbUserData(userData);
                        }
                    });
            },
            error => {
                console.log('FB login error: '+ JSON.stringify(error));
                if(error.errorCode === '4201'){
                  this.events.publish('dismissLoader', 'Error de Facebook', 'Diálogo cancelado', 'Si desea utilizar otra cuenta, debe finalizar la actual en la aplicación de Facebook', true);
                }else{
                  this.events.publish('dismissLoader', 'Error de Facebook', JSON.stringify(error), true);
                }
            });
  }        

  saveFbUserData(userData: any){
    
        //Facebook.api('me?fields=email,name', ['public_profile']).then(
        return this.fbMock.apiCall('fb-user-data.json').subscribe(
                    data => {              
                        let fbUserData = {
                        username: data.email, 
                        nombre: data.name, 
                        id: userData.ID,
                        fbId: data.id,
                        password: '',
                        provider: 'facebook'
                        };
                        console.log('FB profile data => saveFbUserData(): '+ JSON.stringify(data));
                        this.user = fbUserData;
                        console.log('user => saveFbUserData(): '+ JSON.stringify(this.user));
                        this.storageProvider.saveData(this.user);    
                    
                        setTimeout(() => {
                        this.navCtrl.setRoot(DashboardPage, {
                            loginRef: true
                        });
                        }, 400);
                    },
                    error =>{
                        console.log('FB profile error: '+ JSON.stringify(error));
                        this.events.publish('dismissLoader', 'Error de Facebook' ,'Error obteniendo datos de Facebook.', true);
                    });
  } 

}
