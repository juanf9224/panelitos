import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, Events, AlertController, Slides } from 'ionic-angular';
import { SaveUserProvider } from '../../providers/save-user-provider/save-user-provider';
import { IonicStorageProvider } from '../../providers/storage-provider/ionic-storage-provider';
import { getBaseUrl, getUrlCrc } from '../../providers/veo-config-provider/ciause';
 
@Component({
  selector: 'add-user-page',
  //templateUrl: 'add-user-page.html',
  templateUrl: getBaseUrl('ADDUSER') + '&UCRC=' + getUrlCrc('ADDUSER'),
})
export class AddUserPage implements OnInit{
 
    @ViewChild('signupSlider') signupSlider: Slides;
    
    templateTitle: string = 'Añadir Referido'; 
    slideForms: any;
    slideOneForm: FormGroup;
    slideTwoForm: FormGroup;
    slideThreeForm: FormGroup;
    slideFourForm: FormGroup;
    callDays: any = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
    selectOptions: {};
    pickerOpts: {};
    ultData: any;
    globalErrorMsg: string = '';
    submitAttempt: boolean = false;
    slideOneNext: boolean = false;
    slideThreeNext: boolean = false;
    slidesInstance: any;
 
    constructor(
        public navCtrl: NavController, 
        public formBuilder: FormBuilder, 
        public event: Events, 
        public alertCtrl: AlertController, 
        public saveUser: SaveUserProvider,
        public storageProvider: IonicStorageProvider,
        public events: Events) {       
    }

    ngOnInit(){    
        this.signupSlider.paginationType = 'progress'; 
        this.signupSlider._allowSwipeToNext = true;
        this.signupSlider._allowSwipeToPrev = true;
        this.selectOptions = {
            title: 'Día(s) preferible(s) de llamada (Opcional)1',
            cssClass: 'select-box-style'
        };

        this.pickerOpts = {
            title: 'Escoja las horas disponibles',
            cssClass:'picker-opt-style'
        }        

        this.event.publish('dismissLoader');

        this.slideOneForm = this.formBuilder.group({
            firstName: ['', Validators.compose([Validators.minLength(4), Validators.pattern('^[A-Za-z -]*[A-Za-z][A-Za-z -]*$')])],
            lastName: ['', Validators.compose([Validators.minLength(4), Validators.pattern('^[A-Za-z -]*[A-Za-z][A-Za-z -]*$')])],
            pueblo: ['', Validators.compose([Validators.minLength(4), Validators.pattern('^[A-Za-z -]*[A-Za-z][A-Za-z -]*$')])],
        });

        this.slideTwoForm = this.formBuilder.group({
            callDay: ['',Validators.compose([Validators.pattern('^[A-Za-z -\,]*[A-Za-z][A-Za-z -\,]*$')])],
            deHora: ['',Validators.compose([Validators.pattern(/^((\d{1,2}?)|(\d{1})):\d{1,2}$/)])],
            aHora: ['',Validators.compose([Validators.pattern(/^((\d{1,2}?)|(\d{1})):\d{1,2}$/)])]
        });

        this.slideThreeForm = this.formBuilder.group({
            telPrincipal: ['',Validators.compose([ Validators.minLength(7), Validators.maxLength(10), Validators.pattern('^[0-9 -]*[0-9][0-9 -]*$')])],
            telSecundario: ['',Validators.compose([Validators.minLength(7), Validators.maxLength(10), Validators.pattern('^[0-9 -]*[0-9][0-9 -]*$')])],
            telCel: ['',Validators.compose([Validators.minLength(7), Validators.maxLength(10), Validators.pattern('^[0-9 -]*[0-9][0-9 -]*$')])],
            telCasa: ['',Validators.compose([Validators.minLength(7), Validators.maxLength(10), Validators.pattern('^[0-9 -]*[0-9][0-9 -]*$')])]
        });

        this.slideFourForm = this.formBuilder.group({
            emailPrincipal: ['', Validators.compose([Validators.pattern(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)])],
            emailSecundario: ['', Validators.compose([Validators.pattern(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)])],
            emailRepresentante: ['', Validators.compose([Validators.pattern(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)])]
        });

        this.slideForms = [this.slideOneForm, this.slideTwoForm, this.slideThreeForm, this.slideFourForm];

    }

    private getReferer(userData: any, create: boolean, referer: boolean){        
    
        this.storageProvider.loadData('UserData').then(
            data =>{
                let parsedData = JSON.parse(data);                
                let checkData = {
                    checkCasoID: parsedData.id,
                    checkEmail: userData.emails.emailPrincipal
                }
                console.log('checkData', JSON.stringify(checkData), JSON.stringify(data), JSON.stringify(parsedData));
                this.saveData(userData, create, referer, checkData);
            },
            error =>{
                let alertResult = this.alertCtrl.create({
                        title: 'Error en la solicitud',
                        subTitle: JSON.stringify(error),
                        cssClass: 'alert-box',
                        buttons: [
                            {
                                text: 'Cancelar',
                                role: 'cancel'
                            },                                    
                            {
                                text: 'Ok',                                        
                            }
                            ]
                    });

                alertResult.present();
                console.error('Error getting data from storage... ', error);
            }
                
            );
    }

    isSlideNext(activeIndex: number, prevIndex: number): boolean{
        if(activeIndex > prevIndex){
            return true;
        }else{
            return false;
        }
    }
 
    next(slide){
        if(slide === 1){
            this.slideOneNext = true;
        }else{
            this.slideThreeNext = true;
        }
        
        this.signupSlider.slideNext();        
    }
 
    prev(){
        this.signupSlider.slidePrev();
    }

    slideWillChange(event, swiper){
        let actIndex = this.signupSlider.getActiveIndex();
        let prevIndex= this.signupSlider.getPreviousIndex();
        console.log('transition on slide: ', actIndex, prevIndex);
        if(this.isSlideNext(actIndex, prevIndex)){
            console.log('is next slide');
            this.validateSlide(prevIndex);
        }else{
            console.log('is prev or same slide');
        }      
    }

    validateSlide(slideIndex: number){
        try{

        if(slideIndex === 0){
        this.slideOneNext = true;
        }else if(slideIndex === 2){
            this.slideThreeNext = true;
        }

        if(!this.slideForms[slideIndex].valid){
            console.log('invalid');
            this.alertCtrl.create({
                title: 'Aviso',
                message: 'Quedaron campos vacios o invalidos.',
                buttons: [
                    {
                        text: 'Solucionar',
                        handler: () =>{
                            console.log('solucionar');
                            this.signupSlider.slidePrev();
                        }
                    },
                    {
                        text: 'Continuar',
                        handler: () =>{
                            console.log('Continuar');
                        }
                    }
                    ]
            }).present()
        }else{
            console.log('valid');
        }
        }catch(error){
            console.error('Error: ', error);
        }
    }

    checkValidation(){
        this.submitAttempt = true;
        let validCount = 0;
        for(let i = 0; i < this.slideForms.length; i++){
            let slideForm = this.slideForms[i];
            if(!slideForm.valid){
                console.log('invalid: ', i);
                this.signupSlider.slideTo(i);
                break;
            }else{
                validCount++;
                console.log('valid ', i);
                if(validCount >= this.slideForms.length){
                    this.save();
                } 
            }
        }  
    }

    private clearForm(){
        
            this.slideOneForm.reset();
            this.slideTwoForm.reset();
            this.slideThreeForm.reset();
            this.slideFourForm.reset();
            this.signupSlider.slideTo(0);
    }
 
    save(){
        this.events.publish('presentLoader', 'loading');
        this.submitAttempt = true;
        this.ultData = {
            datosGenerales: this.slideOneForm.value,
            prefLlamadas: this.slideTwoForm.value,
            telefonos: this.slideThreeForm.value,
            emails: this.slideFourForm.value
        }
        let calls = this.ultData.prefLlamadas.callDay == undefined || this.ultData.prefLlamadas.callDay === '' ? '' : this.ultData.prefLlamadas.callDay.join(',');
        //let days = '';
        console.log(calls)

        let prettyPrint = 
        '<b>Nombre:</b> ' + this.ultData.datosGenerales.firstName +
        '<br> <b>Apellido:</b> '+ this.ultData.datosGenerales.lastName + 
        '<br> <b>Pueblo:</b> '+ this.ultData.datosGenerales.pueblo +
        '<br> <b>Dias preferibles:</b> '+ calls +
        '<br> <b>De:</b> '+ this.ultData.prefLlamadas.deHora + 
        '<br> <b>A:</b> ' + this.ultData.prefLlamadas.aHora + 
        '<br> <b>Telefono Principal:</b> '+ this.ultData.telefonos.telPrincipal +
        '<br> <b>Telefono Secundario:</b> ' + this.ultData.telefonos.telSecundario +
        '<br> <b>Telefono Celular:</b> ' + this.ultData.telefonos.telCel +
        '<br> <b>Telefono Casa:</b> ' + this.ultData.telefonos.telCasa +
        '<br> <b>Email Principal:</b> ' + this.ultData.emails.emailPrincipal +
        '<br> <b>Email Secundario:</b> ' + this.ultData.emails.emailSecundario +
        '<br> <b>Email Representante:</b> ' + this.ultData.emails.emailRepresentante;

        let alertResult = this.alertCtrl.create({
            title: '',
            subTitle: prettyPrint,
            cssClass: 'alert-box',
            buttons: [
            {
                text: 'Cancelar',
                role: 'cancel',
                handler: () =>{
                    console.log('Operation canceled...');
                }
            },
            {
                text: 'Ok',
                handler: () =>{
                    this.getReferer(this.ultData, false, true);
                }
            }
            ]
        });

        alertResult.present();   

        console.log('ultData: ', JSON.stringify(this.ultData));
    }

saveData(userData: any, create: boolean, referer: boolean, checkData: any){
                this.saveUser.checkIfUserExist(checkData).subscribe(
                    result =>{
                        userData.caso = {referer: checkData.checkCasoID}
                        console.log('Result => saveData() => checkIfUserExist(): ', JSON.stringify(result));
                        if(result.totalrecord < 1){
                            return this.saveUser.saveUser(userData, create, referer).subscribe(
                                    success => {
                                        console.log('sucess saving user: ', JSON.stringify(success));
                                        this.clearForm();
                                        this.events.publish('dismissLoader');
                                    });
                        }else{
                            this.events.publish('dismissLoader', '', 'Este referido ya esta registrado.', true);
                        }
                    },
                    error =>{
                        this.events.publish('dismissLoader', 'Algo salío mal', JSON.stringify(error), true);
                        console.error('Error checking user existence... ', error);
                    }
                );
    }    
 
}