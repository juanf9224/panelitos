import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { getBaseUrl, getUrlCrc } from '../../providers/veo-config-provider/ciause';
import { GraphService } from '../../providers/graph-service/graph-service';

declare var draw;

@Component({
  selector: 'page-graphic-page',
  //templateUrl: 'graphic-page.html',
  templateUrl: getBaseUrl('GRAPHIC') + '&UCRC=' + getUrlCrc('GRAPHIC')
  //template: <amCharts [id]="id" [options]="chartConfig" [style.width.%]="100" [style.height.%]="100"></amCharts>
})
export class GraphicPage {
  providerData: any;
  chartConfig: any;
  charts: any;
  templateTitle: string;
  id: string = "chartdiv";
  userCaso: any;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public events: Events, 
    public graphService: GraphService) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad GraphicPagePage');
    this.providerData = this.navParams.get('item');
    this.userCaso = this.navParams.get('referer');
    setTimeout(() =>{
      this.events.publish('dismissLoader');
      this.drawGraphic(this.providerData.CodeURL);
    }, 300);    
  }

  drawGraphic(param?: string){
    this.graphService.load(param, this.userCaso).subscribe(
      data =>{
        try{
          this.charts = [];
          this.chartConfig = data.responseObject; 
          this.templateTitle = data.PARTIT;
          let pfldc = [];
          pfldc = data.PFLDC.split(',');  
          let container = <HTMLElement> document.getElementById('graphs-container');
          let graph: HTMLElement;
          let graphId = 'graph-';

          setTimeout(() =>{
              for(let chart in this.chartConfig){
                this.charts.push({id: graphId +=chart, chartConf: this.chartConfig[chart]});
                //console.log('AmCharts: ', JSON.stringify(this.chartConfig[chart]), chart, graphId);
                graph = document.createElement('div');
                graph.id = graphId;
                graph.className += pfldc[chart] || 'cgr';                
                container.appendChild(graph);
                draw(graphId, this.chartConfig[chart]);
              }  
          }, 500);     
          
        }catch(error){
          console.error('Error: ', error);
        }
      }, 
      error =>{
        console.error('Error getting chart config. ', error);
      }
    );
  }

}
