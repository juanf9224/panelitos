import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController, Events, ModalController } from 'ionic-angular';
import { getBaseUrl, getUrlCrc } from '../../providers/veo-config-provider/ciause';
import { TableService } from '../../providers/table-service/table-service';
import { TemplateService } from '../../providers/template-service/template-service';
import { MenuDataProvider } from '../../providers/menu-data/menu-data';

@Component({
  selector: 'table-page',
  //templateUrl: 'table-page.html'
  templateUrl: getBaseUrl('TABLE') + '&UCRC=' + getUrlCrc('TABLE')
})

export class TablePage implements OnInit{
  options: any;
  menuId: any;
  menuD: any;
  item: any; 
  keys: any;
  templateService: TemplateService;
  values: Array<any>;
  valuesConfig: any;
  showGrid: boolean = true;
  templateTitle: string;
  colHeaderClass: string;
  userCaso: any;

  constructor(
    public nav: NavController, 
    public navParams: NavParams, 
    public view: ViewController, 
    public events: Events, 
    public menuDataProvider: MenuDataProvider,
    public modalCtrl: ModalController,
    public tableService: TableService
    ) {}

    
  ngOnInit(){
    try{      
        console.log('init... ', this.view.data);
        this.templateService = new TemplateService();
        this.userCaso = this.navParams.get('referer');
        this.item = this.navParams.get('item'); 
        setTimeout(() =>{
          this.loadOptions(this.item.CodeURL, this.userCaso);
        }, 500); 
    
    }catch(error){
        console.error(error);
      }    
  }

  loadOptions(param?:string, referer?: any){ 
      this.tableService.load(param, referer).subscribe(
        data => {
          this.options = data.responseObject;
          let pfldc = [];
          let pfldj = [];
          pfldc = data.PFLDC.split(',');
          pfldj = data.PFLDJ.split(',');
          if(pfldj.length > 2){
            this.colHeaderClass = pfldj.shift();
            console.log('Column header class found... removing... ', pfldj);
          }else{
            this.colHeaderClass = '';
            console.log('no column header class found.');
          }          
          //console.log('col headers: ', this.colHeaderClass);
          this.templateTitle = data.PARTIT;
          //this.valuesConfig = this.getValuesConfig(options);
          this.valuesConfig = {classes: pfldc, rowClasses: pfldj};                            
          //console.log('config: ', JSON.stringify(this.valuesConfig));
          this.menuId = [];
          for(let opt of this.options){
              this.menuId.push({casoID: opt.CasoID, menuId: opt.CALLNEWURL});  
          }          
          this.formatData(this.options);          
          let valuesRowClasses = this.valuesConfig.rowClasses;  
          this.customTableStyle(valuesRowClasses[0], valuesRowClasses[1]);
          //console.log('data: ', JSON.stringify(options));
          //this.processItems(options);
        },
        error => {
          console.error('Error laoding options: ', error);
        }
      );    
  }

  formatData(value: any){
    try{
    this.keys = [];
    this.values = []; 
    let valuesClasses = this.valuesConfig.classes;  
    //console.log('value: ', JSON.stringify(valuesClasses), JSON.stringify(valuesColColors));

    let classes = ''; 
    let colCountK = 0;  

    //Add classes to column headers...
    for(let key in value[0]){
      let objCount = Object.keys(value[0]).length;
      if(colCountK > (objCount -1)){        
          colCountK = 0;
          classes = valuesClasses[colCountK];        
      }else{        
          //console.log('count: else', countV);          
          if(valuesClasses[key] === 'undefined' || valuesClasses[key] === '' || colCountK > (valuesClasses.length - 1)){
            //console.log('undefined or bigger count', colCountK, colCountK);
            classes = 's1 jl';
          }else{
            classes = valuesClasses[colCountK];
          }
      }      
      this.keys.push({key: key, config: classes || ''});        
      colCountK++;                  
    }

    let colCountV = 0;
    //Add classes to column values...
    for(let val in value){  
     // console.log('value length: ', value.length); 
      colCountV = 0;
      for(let v in value[val]){        
        //console.log('value[val] length: ', Object.keys(value[val]).length, countV);
        //let objCount = Object.keys(value[val]).length;        
        if(valuesClasses[v] === 'undefined' || valuesClasses[v] === ' ' || colCountV > (valuesClasses.length - 1)){
          //console.log('bigger count', colCountV, (this.valuesConfig.length - 1));
          classes = 's1 jl';
        }else{
          if(valuesClasses[colCountV] === '' || valuesClasses[colCountV] === ' '){
              classes = 's1 jl';
          }else{          
              classes = valuesClasses[colCountV];
          }
        }        
        //console.log('count out: ', countV, classes);
        //console.log('value[val]: ', val.length, countValueVal++);
        this.values.push({value: value[val][v],  config: classes || ''});
        colCountV++;
        //console.log('config at: ', this.valuesConfig[0]);
      }           
    }
    
    let chunk = this.keys.length;
    let tempArray = [];
    let testArr = [];
    for(let i =0; i<this.values.length; i+=chunk){
      tempArray = this.values.slice(i,i+chunk);
      testArr.push(tempArray);
    }

    this.values = testArr;

    //let testArray = [value[0]];
    //console.log('Object.keys: ', JSON.stringify(tempArray), JSON.stringify(testArr));

    //console.log('keys value: ', JSON.stringify(this.keys));
    //console.log('values: ', JSON.stringify(this.values));
    
    }catch(error){
      console.log('Error: ', error);
    }
  }

  customTableStyle(evenClass?: string, oddClass?: string){
    setTimeout(() =>{      
    //console.log('colors: ', evenClass, evenClass);
    try{
    let oddElement = document.getElementsByClassName('odd');
    for(let i=0; i <= oddElement.length -1; i++){
        //console.log('odd element', oddElement);
        let ele = <HTMLElement>oddElement.item(i);
          ele.className = 'row-style odd '+ oddClass;
        //console.log('ele class name: ', ele.className);
    }

    let evenElement = document.getElementsByClassName('even');
    for(let i=0; i <= evenElement.length -1; i++){
        //console.log('even element', evenElement);
        let ele = <HTMLElement>evenElement.item(i);    
        ele.className = 'row-style even '+ evenClass;
        //console.log('ele class name: ', ele.className);
    }
    this.showGrid = false;
    }catch(error){
      console.log('Error adding row style: ', error);
    }
    }, 300);
  }

  callMenuId(event, row){
    let menu;
    for(let obj in this.menuId){
      if(this.menuId[obj].casoID === row[0].value){
        let menuID = this.menuId[obj].menuId.substring(this.menuId[obj].menuId.indexOf('=')+1, this.menuId[obj].menuId.indexOf('&'));
        menu = {id: this.menuId[obj].casoID, menuId: parseInt(menuID), menuData: this.menuId[obj].menuId, customParam: true};
        console.log('row data', JSON.stringify(menu), menuID);
      }      
    }

   this.menuDataProvider.load(menu.menuData, menu.customParam).subscribe(
     data =>{
        this.menuD = data.responseObject[0]; 
        console.log('Menu Data: ', JSON.stringify(this.menuD));       
        this.templateService.optionTapped(this.menuD).subscribe(
          result =>{
              console.log('Data: ', JSON.stringify(result));
              result.info.CodeURL += '&'+menu.menuData;
              this.presentCustomModal(result);
          },
          error =>{
              console.error('Error: ', error);
          }
        );
     },
     error =>{
        console.error('Error: ', error);
     }
   )
  }

  presentCustomModal(result: any){    
        let customModal = this.modalCtrl.create(result.view, {
          item: result.info,
          showBack: true,
          parentItem: this.item
        });
        customModal.onDidDismiss(() =>{
          console.log('Dismiss ', this.valuesConfig.rowClasses[0]);
          //this.customTableStyle(this.valuesConfig.rowClasses[0], this.valuesConfig.rowClasses[1]);
        });
        customModal.present();
  }

}