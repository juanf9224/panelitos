import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { CiaUseConfig } from '../veo-config-provider/ciause';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class LoginService {
  data: any;

  constructor(private http: Http, private configData: CiaUseConfig) {
    this.data = null;
    this.configData = new CiaUseConfig();
    console.log('Login Service');
  }

  checkIfUserExist(user: any): Observable<any> {
   /* if (this.data) {
      // already loaded data
      //console.log('already loaded');
      return Promise.resolve(this.data);
    }*/
    let fData = this.configData.getCustomValues() + 'WC=Qw.A10&CALL=WEBSERVICE-PANELITO-MAIN&WS=VALIDA-FBUSER&VEOMP=1&veo_FBUID=' + user.userID;    

    // don't have the data yet
    /*return new Promise(resolve => {
      //console.log('new promise');*/
      // We're using Angular Http provider to request the data,
      // then on the response it'll map the JSON data to a parsed JS object.
      // Next we process the data and resolve the promise with the new data.
        return this.http.get(fData + '&UCRC=' + this.configData.simpleCRC(fData.substr(fData.indexOf('?') + 1)))
        .timeout(10000)
        .map(this.extractData)
        .catch(this.handleError);

       /* .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          //console.log('resolve');
          this.data = data;
          resolve(this.data);
        });
    });*/
  }
  
  private extractData(res: Response){
    //console.log('here');
      let body = res.json();
      return body || {};
  }

  private handleError(error: any){
      let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Oops... Algo salio mal';
      console.error(errMsg); // log to console instead
      return Observable.throw(errMsg);
  }

}