import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { CiaUseConfig } from '../veo-config-provider/ciause';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SaveUserProvider {
  data: any;

  constructor(private http: Http, private configData: CiaUseConfig) {
    this.data = null;
    this.configData = new CiaUseConfig();
  }

  checkIfUserExist(user: any): Observable<any> {
   /* if (this.data) {
      // already loaded data
      //console.log('already loaded');
      return Promise.resolve(this.data);
    }*/
    let fData = this.configData.getCustomValues() + 'WC=Qw.A10&CALL=WEBSERVICE-PANELITOS&WS=VALIDA-REFERIDO&VEOMP=1&veo_emailPrincipal=' + user.checkEmail + '&veo_referer=' + user.checkCasoID;   

    // don't have the data yet
    /*return new Promise(resolve => {
      //console.log('new promise');*/
      // We're using Angular Http provider to request the data,
      // then on the response it'll map the JSON data to a parsed JS object.
      // Next we process the data and resolve the promise with the new data.
        return this.http.get(fData + '&UCRC=' + this.configData.simpleCRC(fData.substr(fData.indexOf('?') + 1)))
        .timeout(10000)
        .map(this.extractData)
        .catch(this.handleError);

       /* .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          //console.log('resolve');
          this.data = data;
          resolve(this.data);
        });
    });*/
  }

  saveUser(user: any, create: boolean, referer?: boolean): Observable<any> {
    /*let headers = new Headers(
      {
        "Access-Control-Allow-Origin": '*',
        "Access-Control-Allow-Headers": 'Content-Type',
        "Content-Type": 'text/xml,application/json;charset=iso-8859-1'
       }
      );
    let options = new RequestOptions({ 
                                      headers: headers 
                                    });*/
    let fData = '';
    if(create){
        fData = this.configData.getCustomValues() + 'WC=Qw.A10&CALL=WEBSERVICE-PANELITO-MAIN&WS=MAININFO-PANEL&VEOMP=1' + this.translateData(user);
    }else if(referer){
        fData = this.configData.getCustomValues() + 'WC=Qw.A10&CALL=WEBSERVICE-PANELITOS&WS=MAININFO-PANEL&VEOMP=1' + this.translateData(user);
    }else{
        fData = this.configData.getCustomValues() + 'WC=Qw.A10&CALL=WEBSERVICE-PANELITOS&WS=MAININFO-PANEL&VEOMP=1' + this.translateData(user);
    }                               
    

    return this.http.get(fData + '&UCRC=' + this.configData.simpleCRC(fData.substr(fData.indexOf('?') + 1)))
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  private translateData(user: any): any{
    let keys = [];
    let parsedData = '';
    try{
    for(let item in user){    
      for(let k in user[item]){ 
        keys.push({key: 'veo_'+k, value: user[item][k]});        
      }
    }
    console.log('Data: ', JSON.stringify(keys));


    for(let k of keys){
      if(k.value === undefined || k.value === '' || k.value === ' ' || k.value === null || k.value === 'null'){
        k.value = '';
        parsedData += '&' + k.key + '=' + k.value;
        console.log('Empty value... to blank...: ', k.key);            
      }else{
        parsedData += '&' + k.key + '=' + k.value;
      }
    }

    console.log('parsed Data: ', parsedData);
    }catch(error){
      console.error('Error transalting data: ', error);
      return this.handleError(error);
    }

    return parsedData;
  }

  private extractData(res: Response){
    //console.log('here');
      let body = res.json();
      return body || {};
  }

  private handleError(error: any){
      let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Oops... Algo salio mal';
      console.error(errMsg); // log to console instead
      return Observable.throw(errMsg);
  }
}

