import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { User } from '../../pages/login-page/user';
import { CiaUseConfig } from '../veo-config-provider/ciause';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class FacebookMockProvider {
  data: any;
  constructor(private http: Http, private configData: CiaUseConfig) {
    this.data = null;
    this.configData = new CiaUseConfig();
  }

  apiCall(api:string): Observable<any> {
   /* if (this.data) {
      // already loaded data
      //console.log('already loaded');
      return Promise.resolve(this.data);
    }*/
    /*let fData = '';
    if(api === 'loginStatus'){
        fData = 'fb-login-status';
    }else if(api === 'login'){
        fData = 'fb-login-data';
    }else if(api === 'api'){
        fData = 'fb-user-data';
    }*/
    

    // don't have the data yet
    /*return new Promise(resolve => {
      //console.log('new promise');*/
      // We're using Angular Http provider to request the data,
      // then on the response it'll map the JSON data to a parsed JS object.
      // Next we process the data and resolve the promise with the new data.
        return this.http.get('assets/mocks/'+ api)
        .timeout(10000)
        .map(this.extractData)
        .catch(this.handleError);

       /* .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          //console.log('resolve');
          this.data = data;
          resolve(this.data);
        });
    });*/
  }

  private extractData(res: Response){
    //console.log('here');
      let body = res.json();
      return body || {};
  }

  private handleError(error: any){
      let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Oops... Algo salio mal';
      console.error(errMsg); // log to console instead
      return Observable.throw(errMsg);
  }
}