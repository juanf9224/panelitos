import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Http } from '@angular/http';
import { CiaUseConfig } from '../veo-config-provider/ciause';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/timeout';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class DashboardProvider {

	data: any;
	configData: CiaUseConfig = new CiaUseConfig();
  service: string;

  constructor(private http: Http, public events: Events) {
    console.log('Dashboard Provider');
  }

load(param?: string, referer?: any): Observable<any> {
    if(param != undefined && param.length > 0){
      console.log('param found: ', param);
      if(this.data && this.data.service == param) {
        console.log('param already loaded: ', param);        
        // already loaded data
        return Observable.of(this.data);
      }else{
      this.service = param;
      }
    }else{
      if(this.data && this.data.service == 'NEWENERGY') {
        console.log('already loaded');         
        // already loaded data
        return Observable.of(this.data);
      }else{
        this.service = 'NEWENERGY';
      }
    }

    // don't have the data yet
    let fData = this.configData.getValues() + 'GETMENU&PAR01=' + this.service +'&LOGUSER='+ referer.id;
    // fData + '&UCRC=' + this.configData.simpleCRC(fData.substr(fData.indexOf('?') + 1))
    // assets/mocks/dashboard-mock.json
    
    return this.http.get(fData + '&UCRC=' + this.configData.simpleCRC(fData.substr(fData.indexOf('?') + 1)))
        .timeout(10000)
        .map(res => res.json())
        .do(val => {
            this.data = val;
            this.data.service = this.service;
          })
        .first();
}

}