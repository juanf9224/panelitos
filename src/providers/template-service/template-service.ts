import { Injectable } from '@angular/core';
import { TablePage } from '../../pages/table-page/table-page';
import { TableFormPage } from '../../pages/table-form-page/table-form-page';
import { GraphicPage } from '../../pages/graphic-page/graphic-page';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TemplateService {
  data: any;
  api: any;  

  constructor() {
      console.log('Template Service');
  }  
  
  public optionTapped(item: any): Observable<any>{
      console.log('item data: ', item.MenuCallType, (item.MenuCallType === 'TABLEFORM'));
      if(item.MenuCallType != 'NEWMENU'){
          if(item.MenuCallType === 'TABLE'){
              console.log('item data TABLE: ', JSON.stringify(item));  
              this.data = {view: TablePage, info: item};
          }else if(item.MenuCallType === 'TABLEFORM'){ 
              console.log('item data TABLEFORM: ', JSON.stringify(item));
              this.data = {view: TableFormPage, info: item};
          }else if(item.MenuCallType === 'GRAPHIC'){ 
              console.log('item data GRAPHIC: ', JSON.stringify(item));
              this.data = {view: GraphicPage, info: item};
          }          
          return Observable.of(this.data);
      }else{
          return Observable.of(false);
      }
  }
}