import { Platform } from 'ionic-angular';

export function getBaseUrl(template?: string){
    return CiaUseConfig.getValuesS() + template + '&GETTEMPLATE=1';    
}

export function getUrlCrc(template?: string){
    return CiaUseConfig.simpleCRCS((getBaseUrl(template)).substr((getBaseUrl(template)).indexOf('?') + 1));
}

//http://hostseven.lq3.net:8091 http://192.168.1.120:8090/veocrm/webservice/call_webservice.asp?VEOCIACRC=72BO145&USERNAME=hostseven&password=qwerty01&CALL=WEBSERVICE-APPMENU&WC=Qw.A10&WS=GETMENU&PAR01=NEWENERGY
//http://192.168.1.15     webservice user password - hostseven qwerty01a
export class CiaUseConfig{
    public static platform: Platform = new Platform();
    constructor(){        
    }

    public static V_USER: string = 'hostseventest';
    public static V_PWD: string  = 'qwerty01a';

    public static V_CIACRC: string = '77A9O151F';
    public static V_TOKEN: string = 'Qw.A10';

    public static V_CONTROL_CRC: string ='';
    public static V_INIT_URL: string = 'http://call.codecrm.com';
    public static V_IMAGE_REPOSITORY_LQ3: string = 'http://192.168.1.15/veocrm/attachdoc/appmenu/veo_07';
    public static V_IMAGE_REPOSITORY: string = 'http://call.codecrm.com';
    public static V_GENERAL_URL = '/VeoCRM/webservice/call_webservice.asp?VEOCIACRC=';
    public static V_POST_URL = '/VeoCRM/webservice/call_webserviceHTTP.asp?VEOCIACRC=';

    public static V_FOLDER_IMAGE: string = "1";
    public static V_WEB_SERVICE: string = "&CALL=WEBSERVICE-APPMENU";

    getValues(): string{
        let SERVICE_CALL = CiaUseConfig.V_INIT_URL + '/VeoCRM/webservice/call_webservice.asp?VEOCIACRC=' + CiaUseConfig.V_CIACRC + '&USERNAME=' + CiaUseConfig.V_USER + '&PASSWORD=' + CiaUseConfig.V_PWD + '&WC=' + CiaUseConfig.V_TOKEN + CiaUseConfig.V_WEB_SERVICE +'&WS=';
        return SERVICE_CALL;
    }

    getPostValues(): string{
        let SERVICE_CALL = CiaUseConfig.V_INIT_URL + CiaUseConfig.V_POST_URL + CiaUseConfig.V_CIACRC + '&USERNAME=' + CiaUseConfig.V_USER + '&PASSWORD=' + CiaUseConfig.V_PWD + '&WC=' + CiaUseConfig.V_TOKEN + CiaUseConfig.V_WEB_SERVICE +'&WS=';
        return SERVICE_CALL;
    }

    public static getValuesS(): string{
        let SERVICE_CALL = this.V_INIT_URL + '/VeoCRM/webservice/call_webservice.asp?VEOCIACRC=' + this.V_CIACRC + '&USERNAME=' + this.V_USER + '&PASSWORD=' + this.V_PWD + '&WC=' + this.V_TOKEN + this.V_WEB_SERVICE +'&WS=';
        return SERVICE_CALL;
    }

    getCustomValues(){
        let SERVICE_CALL = CiaUseConfig.V_INIT_URL + CiaUseConfig.V_GENERAL_URL + CiaUseConfig.V_CIACRC + '&USERNAME=' + CiaUseConfig.V_USER + '&PASSWORD=' + CiaUseConfig.V_PWD + '&';
        return SERVICE_CALL;
    }

    simpleCRC(vSTR) {
        let x, vS, vM, vX, strORG, hKey2;
        vSTR = vSTR.trim();
        strORG = vSTR;
        vS = 0;
        let hKey1 = 171;
        let date = new Date();
        hKey2 = date.getFullYear() + '' + (date.getMonth() + 1) + '' + date.getDate();
        /*console.log('url:' + vSTR);
        console.log('key1:' + hKey1);
        console.log('combination year-day: ' + hKey2);*/

        vSTR = hKey1 + '' + hKey2 + '' + vSTR;

        //console.log('vSTR: ' + vSTR);

        for (x = 0; x < vSTR.length; x = x + 2) {
            vX = ((x + 1) % 2) + 1;
            vM = (vSTR.substr(x, 1).charCodeAt() % 5) + 1;
            vS = vS + ((vSTR.substr(x, 1).charCodeAt()) * (vM + vX));
            //console.log('x:' + x + '   vSTR substr: ' + vSTR.substr(x, 1) + '  vSTR CharCode: ' + vSTR.substr(x, 1).charCodeAt() + '  vM1: ' + vM.toString() + ' vS1: ' + vS.toString() + ' vX: ' + vX.toString() + '\n');
        }

        //console.log('vSF1: ' + vS);

        vSTR = "HOST" + hKey1 + '' + hKey2 + "SEVEN" + strORG;
        //console.log('vSTR: ' + vSTR);
        for (x = 0; x < vSTR.length; x++) {
            vX = ((x + 1) % 2) + 1;
            //charCodeAt()
            vM = (vSTR.substr(x, 1).charCodeAt() % 3) + 1;
            vS = vS + ((vSTR.substr(x, 1).charCodeAt()) * (vM + vX));
        // console.log('x:' + x + '   vSTR substr: ' + vSTR.substr(x, 1) + '  vSTR CharCode: ' + vSTR.substr(x, 1).charCodeAt() + '  vM1: ' + vM.toString() + ' vS1: ' + vS.toString() + ' vX: ' + vX.toString() + '\n');
        }

        //console.log('vSF2: ' + vS);

        let v1 = (vS % 67).toString();
        let v2 = vS.toString();
        let v3 = (vS % 167).toString();
        //console.log(v1, v2, v3);
        vS = v1 + v2 + v3;

        //console.log('vS: ' + vS);

        let veoSimpleCRC = vS;
        for(let i = 0; i <= vS.length; i++){
            if(veoSimpleCRC.indexOf('0')>=0){
                veoSimpleCRC = veoSimpleCRC.replace('0','');
                //console.log(vS);
            }
        }
        //console.log('veoSimpleCRC url: '+ veoSimpleCRC.replace('0','') + '\nveoSimpleCRC: '+ veoSimpleCRC);
        return veoSimpleCRC;

    };

    public static simpleCRCS(vSTR) {
        let x, vS, vM, vX, strORG, hKey2;
        vSTR = vSTR.trim();
        strORG = vSTR;
        vS = 0;
        let hKey1 = 171;
        let date = new Date();
        hKey2 = date.getFullYear() + '' + (date.getMonth() + 1) + '' + date.getDate();
        /*console.log('url:' + vSTR);
        console.log('key1:' + hKey1);
        console.log('combination year-day: ' + hKey2);*/

        vSTR = hKey1 + '' + hKey2 + '' + vSTR;

        //console.log('vSTR: ' + vSTR);

        for (x = 0; x < vSTR.length; x = x + 2) {
            vX = ((x + 1) % 2) + 1;
            vM = (vSTR.substr(x, 1).charCodeAt() % 5) + 1;
            vS = vS + ((vSTR.substr(x, 1).charCodeAt()) * (vM + vX));
            //console.log('x:' + x + '   vSTR substr: ' + vSTR.substr(x, 1) + '  vSTR CharCode: ' + vSTR.substr(x, 1).charCodeAt() + '  vM1: ' + vM.toString() + ' vS1: ' + vS.toString() + ' vX: ' + vX.toString() + '\n');
        }

        //console.log('vSF1: ' + vS);

        vSTR = "HOST" + hKey1 + '' + hKey2 + "SEVEN" + strORG;
        //console.log('vSTR: ' + vSTR);
        for (x = 0; x < vSTR.length; x++) {
            vX = ((x + 1) % 2) + 1;
            //charCodeAt()
            vM = (vSTR.substr(x, 1).charCodeAt() % 3) + 1;
            vS = vS + ((vSTR.substr(x, 1).charCodeAt()) * (vM + vX));
        // console.log('x:' + x + '   vSTR substr: ' + vSTR.substr(x, 1) + '  vSTR CharCode: ' + vSTR.substr(x, 1).charCodeAt() + '  vM1: ' + vM.toString() + ' vS1: ' + vS.toString() + ' vX: ' + vX.toString() + '\n');
        }

        //console.log('vSF2: ' + vS);

        let v1 = (vS % 67).toString();
        let v2 = vS.toString();
        let v3 = (vS % 167).toString();
        //console.log(v1, v2, v3);
        vS = v1 + v2 + v3;

        //console.log('vS: ' + vS);

        let veoSimpleCRC = vS;
        for(let i = 0; i <= vS.length; i++){
            if(veoSimpleCRC.indexOf('0')>=0){
                veoSimpleCRC = veoSimpleCRC.replace('0','');
                //console.log(vS);
            }
        }
        //console.log('veoSimpleCRC url: '+ veoSimpleCRC.replace('0','') + '\nveoSimpleCRC: '+ veoSimpleCRC);
        return veoSimpleCRC;

    };

    encryptHS(user, password) {
        let CRC1, CRC2, YourNUMBER;

        YourNUMBER = 3552;
        //console.log(user, password);
        let uUser = user.toUpperCase();
        let uPassword = password;

        CRC1 = 0;
        CRC2 = 0;
        for (let x = 0; x < uUser.length; x++) {
            CRC1 = CRC1 + (uUser.substr(x, 1).charCodeAt()) * (x + 1 + 2);
            //console.log('CRC 1: '+ CRC1 + ' asKey: '+ uUser.substr(x, 1).charCodeAt());
        }
        for (let x = 0; x < password.length; x++) {
            CRC2 = CRC2 + (uPassword.substr(x, 1).charCodeAt()) * (x + 1 + 3);
            //console.log('CRC 2: '+ CRC2 +' Askey: '+ uPassword.substr(x, 1).charCodeAt());
        }

        // console.log('Datos de encriptacion separados3: '+ CRC1, CRC2, YourNUMBER);
        let dll_EncryptPassword = (uUser + '-' + (CRC1 + CRC2 + YourNUMBER) + '' + ((CRC2 + CRC1 * YourNUMBER)).toString(16)).toUpperCase();
        return dll_EncryptPassword;
    }
}
