import { Injectable } from '@angular/core';
import { Facebook } from '@ionic-native/Facebook';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SocialProvider {
    data: any;
    constructor(){
        this.data = null;
    }

    fbLogin(): Observable<any>{
        if(this.data){
            console.log('already loaded data');
            return this.data;
        }else{
        
            return Observable.fromPromise(new Promise(resolve => {

                return Facebook.login(['email']).then(response => {
                    resolve(response);
                }).catch(error => {
                    return Promise.reject(error);
                });

            })
        )
        .map(res => res)
        .do(val => {this.data = val})
        .first()
        .catch(this.handleError);
        }
    }

    getFbProfile(): Observable<any>{
        if(this.data){
            console.log('already loaded data');
            return this.data;
        }else{
        
            return Observable.fromPromise(new Promise(resolve => {

                return Facebook.api('me?fields=email,name', ['public_profile']).then(response => {
                     resolve(response);
                }).catch(error => {
                    return Promise.reject(error);
                });

            })
        )
        .map(res => res)
        .do(val => {this.data = val})
        .first()
        .catch(this.handleError);
        }
    }

    fbLoginStatus(): Observable<any>{
        if(this.data){
            console.log('already loaded data');
            return this.data;
        }else{
        
            return Observable.fromPromise(new Promise(resolve => {

                return Facebook.getLoginStatus().then(response => {
                    resolve(response);
                }).catch(error => {
                    return Promise.reject(error);
                });

            })
        )
        .map(res => res)
        .do(val => {this.data = val})
        .first()
        .catch(this.handleError);
        }
    }

    fbLogout(){
        Facebook.logout().then(response => {
            console.log('Fb logout: '+ JSON.stringify(response));
        }).catch(error =>{
            console.log('Fb logout error: '+ JSON.stringify(error));
        });
    }

    private handleError(error: any){
        let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Oops... Algo salio mal';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }


}