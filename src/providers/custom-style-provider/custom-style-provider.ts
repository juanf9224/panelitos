import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Http } from '@angular/http';
import { CiaUseConfig } from '../veo-config-provider/ciause';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

/*
  Generated class for the ServicesListProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CustomStylesProvider {
	data: any;
	configData: CiaUseConfig = new CiaUseConfig();

  constructor(private http: Http, public events: Events) {
      this.data = null;
  }



load(): Observable<any> {
    if (this.data) {
      // already loaded data
      return Observable.of(this.data);
    }

    // don't have the data yet
    let fData = this.configData.getValues() + 'CUSTOM-STYLE&GETTEMPLATE=1';
    return this.http.get(fData + '&UCRC=' + this.configData.simpleCRC(fData.substr(fData.indexOf('?') + 1)))
        .timeout(6000)
        .map(res => res)
        .do(val => {this.data = val})
        .first();        
        
}
}