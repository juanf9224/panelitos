import { Injectable } from '@angular/core';
import { ToastController, Events} from 'ionic-angular';
import { Storage } from '@ionic/storage';


@Injectable()
export class IonicStorageProvider {

storage: Storage = new Storage();
USER_DATA = 'UserData';
LOGIN_STATE = 'logged';
FB_LOGIN_STATE = 'fbLoginState';
logged: boolean;
public static newLogin: boolean;

  constructor(private events: Events, private toastCtrl: ToastController) {
   
  }

   saveData(data: any){
  	this.storage.set(this.USER_DATA, data)
      .then((response) => {

          //let username = response.username;
          console.log('Response storage provider: '+ /*response.provider, response.username,*/ JSON.stringify(response));
          if(response.provider != 'facebook'){
              console.log('not facebook login', response.provider);
                //this.login(username); 
          }
           
      }).catch(error => {
          let toast = this.toastCtrl.create({
          message: 'Error=> '+ error,
          duration: 2000,
          position: 'bottom'
          });

          toast.present();

          console.error(error);
      });
  }

  loadData(db: string){
    return this.storage.get(db).then(dataP => {
        let data = JSON.stringify(dataP);
        if(data !== 'null' || data !== null || data !== undefined){  
            console.log(' success loadData() => ', data);
            return data;
        }else{
            console.log(' nothing loadData() => ', data);
            return dataP;
        }
    }).catch(error =>{
        return console.error('Error loading data: ', error);
    })
  }

  login(username?: any){
    this.storage.set(this.LOGIN_STATE, true).then(
        success => this.events.publish('user:loginFB', 'native login'),
        error => this.events.publish('user:loginError', error)
      );      
  }

  loginFb(loginData: any){
    this.storage.set(this.FB_LOGIN_STATE, loginData).then(
        success => this.events.publish('user:loginFB', loginData.userID),
        error => this.events.publish('user:loginError', error)
      );      
  }

  logout(){    
   this.storage.remove(this.USER_DATA).then(
      success => {
        this.storage.remove(this.LOGIN_STATE).then(
          success =>
              this.events.publish('user:logout'), error => console.error(error)
        );        
      }).catch(error =>{
        console.error(error);
      });   

      this.storage.clear();     	
  }

  checkLoginState(loginType?: string){
  	return this.storage.get(this.LOGIN_STATE).then(data => {
       if(data !== null || data !== undefined){   
            return data;
       }else{
           return false;
       }
  		},
          error => {
              console.log('checkLoginError '+ JSON.stringify(error));
          });             	
  }

}

