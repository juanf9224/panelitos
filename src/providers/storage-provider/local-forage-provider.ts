import { Injectable } from '@angular/core';
import { ToastController, Events } from 'ionic-angular';
import localforage from 'localforage';


/*
  Generated class for the SecureStorageProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LocalForageStorageProvider {

BLUE_USER_DATA = 'blueUserData';
LOGIN_STATE = 'logged';

  constructor(private events: Events, private toastCtrl: ToastController) {
   
  }

  saveData(data: any){
  	localforage.setItem<any>(this.BLUE_USER_DATA, JSON.stringify(data))
      .then(response => {
          let toast = this.toastCtrl.create({
          message: 'Iniciaste sesión con el Blue Number: '+ data.blueNum,
          duration: 2000,
          position: 'bottom'
    });
          this.login();
    toast.present(toast);
  
      }).catch(error => {
          let toast = this.toastCtrl.create({
          message: 'Error=> '+ error,
          duration: 2000,
          position: 'bottom'
    });

    toast.present();
      })
  }

  loadData(db: string){
    return localforage.getItem<any>(db).then(data => {
        return JSON.parse(data);
    }).catch(error =>{
        return console.error(error);
    })
  }

  login(){
    localforage.setItem<boolean>(this.LOGIN_STATE, true).then(
        success => this.events.publish('user:login'),
        error => console.log('Error: '+ error)
      );
  }

  logout(){
   localforage.removeItem(this.LOGIN_STATE).then(
      success => {
        localforage.removeItem(this.BLUE_USER_DATA).then(
          success => {this.events.publish('user:logout'); JSON.stringify(success); },
          error => console.error(error)
          );        
      }).catch(error =>{
        console.error(error);
      });        	
  }

  checkLoginState(){
  	return localforage.getItem<boolean>(this.LOGIN_STATE).then(data => {
      console.log('Data: '+ data);
  		return data;
  		});  	
  }

}