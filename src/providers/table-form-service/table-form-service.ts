import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Http } from '@angular/http';
import { CiaUseConfig } from '../veo-config-provider/ciause';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/timeout';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class TableFormService {

	data: any;
	configData: CiaUseConfig = new CiaUseConfig();
  api: string;


  constructor(private http: Http, public events: Events) {
    console.log('Table Form Service');
  }

load(param?: string, referer?: any): Observable<any> {

  try{
    if(param != undefined && param.length > 0){
      console.log('param found: ', param);
      if(this.data && this.data.api == param) {
        console.log('param already loaded: ', param);        
        // already loaded data
        return Observable.of(this.data);
      }else{
        this.api = param;
      }
  }else{
      if(this.data) {
        console.log('already loaded');         
        // already loaded data
        return Observable.of(this.data);
      }
    }    

    // don't have the data yet
    let fData = this.configData.getCustomValues() + this.api +'&LOGUSER='+ referer.id;    
    // fData + '&UCRC=' + this.configData.simpleCRC(fData.substr(fData.indexOf('?') + 1))
    // assets/mocks/casos-mock.json
        return this.http.get(fData + '&UCRC=' + this.configData.simpleCRC(fData.substr(fData.indexOf('?') + 1)))
            .timeout(10000)
            .map(res => res.json())
            .do(val => {
                this.data = val;
                this.data.api = this.api;
              })
            .first();
  }catch(error){
      console.error('Error getting WS data: ', error);
  }
}

}